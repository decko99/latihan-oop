<?php
    
    // require ('animal.php'); 
    require ('frog.php');
    include ('ape.php'); 

    echo "<br>";
    echo "Release 0 : ";
    echo "<br>";

    $sheep = new Animal("shaun");

    echo "Nama binatangnya adalah : " . $sheep -> name ;
    echo "<br>";
    echo "Dia mempunyai " . $sheep -> legs . " kaki";
    echo "<br>";
    echo "Apakah dia berdarah panas? " . $sheep -> cold_blooded;

    echo "<br>";
    echo "<br>";
    echo "======================================";
    echo "<br>";
    echo "Release 1 : ";
    echo "<br>";
    echo "<br>";
    
    echo "Ini Class Ape : <br>";

    $sungokong = new Ape("kera sakti");
    echo "Namanya adalah : " . $sungokong -> name . "<br>";
    echo "Dia mempunyai : " . $sungokong -> legs . " kaki. <br>";
    echo "Ciri khasnya adalah : "; 
    echo $sungokong->yell(); // "Auooo"
    
    echo "<br>";
    echo "<br>";
    echo "-------------------------------------";
    echo "<br>";
    echo "<br>";
    
    echo "Ini Class Frog : <br>";
    $kodok = new Frog("buduk");
    echo "Namanya adalah : " . $kodok -> name . "<br>";
    echo "Dia mempunyai : " . $kodok -> legs . " kaki. <br>";
    echo "Ciri khasnya adalah : "; 
    echo $kodok->jump(); // "hop hop"

?>